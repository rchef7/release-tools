# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::ProductMilestone do
  let(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }

  def stub_active_version(value)
    schedule = instance_double(
      ReleaseTools::ReleaseManagers::Schedule,
      active_version: value
    )

    allow(ReleaseTools::ReleaseManagers::Schedule).to receive(:new)
      .and_return(schedule)
  end

  describe '.current' do
    it 'returns the current milestone' do
      current = ReleaseTools::Version.new('14.9')

      stub_active_version(current)

      expect(fake_client).to receive(:group_milestones)
        .with('gitlab-org', hash_including(title: '14.9'))
        .and_return(%i[current])

      expect(described_class.current).to eq(:current)
    end
  end

  describe '.after' do
    it 'returns the next minor milestone' do
      version = ReleaseTools::Version.new('14.9')

      expect(fake_client).to receive(:group_milestones)
        .with('gitlab-org', hash_including(title: '14.10'))
        .and_return(%i[next_minor])

      expect(described_class.after(version)).to eq(:next_minor)
    end

    it 'returns the next major milestone when the next minor is unavailable' do
      version = ReleaseTools::Version.new('14.10')

      expect(fake_client).to receive(:group_milestones)
        .with('gitlab-org', hash_including(title: '14.11'))
        .and_return([])

      expect(fake_client).to receive(:group_milestones)
        .with('gitlab-org', hash_including(title: '15.0'))
        .and_return(%i[next_major])

      expect(described_class.after(version)).to eq(:next_major)
    end
  end

  describe '.next' do
    it 'returns the milestone after the current one' do
      current = ReleaseTools::Version.new('14.9')
      stub_active_version(current)

      allow(fake_client).to receive(:group_milestones)
        .with('gitlab-org', hash_including(title: '14.10'))
        .and_return(%i[next_minor])

      expect(described_class.next).to eq(:next_minor)
    end
  end
end
