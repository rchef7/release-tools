# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockersReport do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:blockers) { stub_const('ReleaseTools::Deployments::Blockers', spy) }

  let(:blocker1) { double(:blocker) }
  let(:blocker2) { double(:blocker) }

  subject(:report) do
    described_class.new
  end

  before do
    allow(blockers)
      .to receive(:deployment_blockers)
      .and_return([blocker1, blocker2])
  end

  describe '#create' do
    before do
      allow(blockers)
        .to receive(:fetch)
        .and_return(nil)

      allow(client)
        .to receive(:create_deployment_blockers_report)
        .and_return(nil)

      allow(report)
        .to receive(:assignees)
        .and_return([1, 2])
    end

    it 'creates an issue' do
      expect(client)
        .to receive(:create_issue)
        .with(
          report,
          ReleaseTools::Project::Release::Tasks
        )

      report.create
    end
  end
end
