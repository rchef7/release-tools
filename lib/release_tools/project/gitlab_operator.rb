# frozen_string_literal: true

module ReleaseTools
  module Project
    class GitlabOperator < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-org/cloud-native/gitlab-operator.git',
        dev:       'git@dev.gitlab.org:gitlab/cloud-native/gitlab-operator.git',
        security:  'git@gitlab.com:gitlab-org/security/cloud-native/gitlab-operator.git'
      }.freeze
    end
  end
end
