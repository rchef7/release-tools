# frozen_string_literal: true

require 'http'

module ReleaseTools
  module ReleaseManagers
    # A client for https://api.slack.com/methods
    class SlackClient
      include ::SemanticLogger::Loggable

      attr_reader :sync_errors, :token, :endpoint

      SLACK_URL = 'https://slack.com/api'

      # Initialize a slack API client specific to Release Manager tasks
      def initialize
        @token = ENV.fetch('SLACK_APP_ADMIN_TOKEN') do |name|
          raise "Missing environment variable `#{name}`"
        end

        @sync_errors = []
      end

      def sync_membership(user_ids)
        logger.info('Syncing membership', user_ids: user_ids, url: user_group_update_url)

        data = usergroup_data.merge(users: user_ids.join(','))

        response = client.post(user_group_update_url, form: data)
        parse_response(response)
      rescue Client::SyncError => ex
        sync_errors << ex
      end

      def members
        logger.info('Fetching membership', url: user_group_list_url)

        response = client.post(user_group_list_url, form: usergroup_data)
        resp = parse_response(response)

        resp['users']
      end

      def join(user_id)
        current_members = members
        if current_members.include?(user_id)
          logger.warn('User is already part of the group', user_id: user_id, members: current_members)
          return
        end

        sync_membership(current_members + [user_id])
      end

      def leave(user_id)
        current_members = members
        unless current_members.include?(user_id)
          logger.warn('User is not part of the group', user_id: user_id, members: current_members)
          return
        end

        sync_membership(current_members - [user_id])
      end

      private

      # https://api.slack.com/methods/usergroups.users.update
      def user_group_update_url
        "#{SLACK_URL}/usergroups.users.update"
      end

      # https://api.slack.com/methods/usergroups.users.list
      def user_group_list_url
        "#{SLACK_URL}/usergroups.users.list"
      end

      def client
        HTTP.auth("Bearer #{@token}").headers('Content-Type': 'application/x-www-form-urlencoded')
      end

      def usergroup_data
        {
          usergroup: ReleaseTools::Slack::RELEASE_MANAGERS,
          team_id: ReleaseTools::Slack::TEAM
        }
      end

      def parse_response(response)
        unless response.status.success?
          case response.status.to_sym
          when :unauthorized
            raise Client::UnauthorizedError, 'Unauthorized'
          when :forbidden
            raise Client::UnauthorizedError, 'Invalid token'
          else
            raise Client::SyncError, response.status.reason
          end
        end

        resp = JSON.parse(response.body)
        unless resp['ok']
          logger.error('Slack release-managers group update failed', response_body: resp)
          raise Client::SyncError, resp['error'] || 'unknown error'
        end

        resp
      end
    end
  end
end
