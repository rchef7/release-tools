package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Histogram interface {
	Metadata
	Observe(value float64, labels ...string)
}

type histogramVec struct {
	*description

	metric *prometheus.HistogramVec
}

func NewHistogramVec(opts ...MetricOption) (Histogram, error) {
	descOpts := applyMetricOptions(opts)

	prom := promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: descOpts.Namespace(),
			Subsystem: descOpts.Subsystem(),
			Name:      descOpts.Name(),
			Help:      descOpts.help,
			Buckets:   descOpts.buckets,
		}, descOpts.labelsNames())

	for _, labels := range descOpts.labelsToInitialize {
		_, e := prom.GetMetricWithLabelValues(labels...)
		if e != nil {
			return nil, e
		}
	}

	return &histogramVec{
		description: descOpts.description,
		metric:      prom,
	}, nil
}

func (c *histogramVec) Observe(value float64, labels ...string) {
	c.metric.WithLabelValues(labels...).Observe(value)
}
