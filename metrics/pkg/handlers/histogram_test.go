package handlers

import (
	"net/http"
	"testing"
)

type mockHistogram struct {
	mockMetric
	value float64
}

func (m *mockHistogram) Observe(value float64, labels ...string) {
	m.value = value
}

func TestHistogramVecHandler(t *testing.T) {
	meta := &mockHistogram{}
	meta.expectedLabels = []string{"green", "medium", "pre-production"}
	handler := NewHistogram(meta).(*histogram)

	req, err := meta.apiRequest("observe", "11.1")
	if err != nil {
		t.Fatal(err)
	}

	rr := testRequest(req, handler)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if meta.value != 11.1 {
		t.Errorf("Metrics not incremented: expected 1 got %v", meta.value)
	}
}
